package barRobot.querysensor;

import com.ergotech.brickpi.BrickPi;
import com.ergotech.brickpi.motion.MotorPort;
import com.ergotech.brickpi.sensors.SensorPort;

import barRobot.robotControl.MoveRobot;

public class QuerySensor implements Runnable {

	private static BrickPi brick = BrickPi.getBrickPi();
	private static int move = 0;
	private static int pump = 0;
	private static int pumpControl = 0;
	private static int touchSensor = 0;
	private static int glasSensor = 0;
	private static int oldtouchSensor = 0;
	private static String[] drinkStation = { "Start","Jacky","BTW J&C","Captain","BTW C&G","Gin","BTW G&T","TonicWater","Ende"};
	/**
	 * set manuel: 0: Start position 1: First drink station 2: Second drink station
	 * etc.
	 */
	private static int position = 0;
	private static int printValues = 0;

	@Override
	public void run() {
		while (true) {

			move = brick.getMotor(MotorPort.MD).getCurrentEncoderValue();
			pump = brick.getMotor(MotorPort.MB).getCurrentEncoderValue();
			pumpControl = brick.getMotor(MotorPort.MC).getCurrentEncoderValue();
			// pressed is 1, less is 0
			if (brick.getSensor(SensorPort.S3).getValue() < 800) {
				touchSensor = 1;
			} else {
				touchSensor = 0;
			}
			if (brick.getSensor(SensorPort.S4).getValue() < 800) {
				glasSensor = 1;
			} else {
				glasSensor = 0;
			}
			// check if toggle TouchSensor
			if (oldtouchSensor != touchSensor) {
				oldtouchSensor = touchSensor;
				if (oldtouchSensor == 1) {
					if (MoveRobot.getLastMovedDirection().equals("forward")) {
						setPosition(position + 1);
					} else {
						setPosition(position - 1);
					}
				} else {
					if(MoveRobot.getLastMovedDirection().equals("forward")) {
						setPosition(position+1);
					} else {
						setPosition(position-1);
					}
				}
			}

			if (printValues == 20) {// print every second
				//System.out.println(toString());
				printValues = 0;
			} else {
				printValues++;
			}

			try {
				Thread.sleep(50); // check every 50ms
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public static int getPosition() {
		return position;
	}

	public static void setPosition(int position) {
		if (position >= 0) {
			QuerySensor.position = position;
		}
	}

	public static int getMove() {
		return move;
	}

	public static int getPump() {
		return pump;
	}

	public static int getPumpControl() {
		return pumpControl;
	}

	public static int getTouchSensor() {
		return touchSensor;
	}
	
	public static int getGlasSensor() {
		return glasSensor;
	}

	public static void resetPumpEncoder() {
		brick.getMotor(MotorPort.MB).resetEncoder();
		pump = 0;
	}

	@Override
	public String toString() {
		return "QuerySensor [\n move=" + move + "\n pump=" + pump + "\n pumpControl=" + pumpControl + "\n GlasSensor= "+glasSensor+ "\n touchSensor= "
				+ touchSensor + "\n" + "\n Drink Station: " + drinkStation[position] + "]";
	}

}
