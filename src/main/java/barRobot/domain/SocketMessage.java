package barRobot.domain;

/**
 * Created by z003dn9v on 26.07.2017.
 */
public class SocketMessage {
	private String content;

	public SocketMessage() {
	}

	public SocketMessage(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
