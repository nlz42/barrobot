package barRobot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import barRobot.domain.SocketMessage;
import barRobot.robotControl.Barkeeper;
import barRobot.robotControl.MoveRobot;



@Controller
public class RobotRestController {
	
	@Autowired
	private SimpMessageSendingOperations messagingTemplate;
	
	/**
	 * Startpage
	 * @param model
	 * @return
	 */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
    	Barkeeper.setMessagingTemplate(messagingTemplate);
        return "index";
    }	
    
    @RequestMapping(value = "/getBusy", method = RequestMethod.GET)
    public ResponseEntity<String> getBusyState() {
        return new ResponseEntity<>(Boolean.toString(Barkeeper.busy), HttpStatus.OK);
    }
    
	/**
	 * Stomp Message Broker for Websocket Connection
	 * @param msg
	 * @return
	 * @throws Exception
	 */
	@MessageMapping("/stompMessage") // "/app/direction"
    @SendTo("/topic/stompMessage")
    public SocketMessage stompMessenger(SocketMessage msg) throws Exception {
		if(msg.getContent().length()>1 && !(msg.getContent().equals("stop"))){ // no direction input
			Barkeeper.setDrinkOrder(msg.getContent());
		} else {
			MoveRobot.move(msg.getContent());
		}
    	return new SocketMessage("Something");
    }
}
