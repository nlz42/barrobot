package barRobot.configuration;

import java.io.IOException;

import com.ergotech.brickpi.BrickPi;
import com.ergotech.brickpi.motion.Motor;
import com.ergotech.brickpi.motion.MotorPort;
import com.ergotech.brickpi.sensors.Sensor;
import com.ergotech.brickpi.sensors.SensorPort;
import com.ergotech.brickpi.sensors.SensorType;

public class RobotPortConfiguration {
	private static BrickPi brick;
	public static Motor move;
	public static Motor pump;
	public static Motor pumpControl;
	public static Sensor touchSensor;
	public static Sensor glasSensor;

	public static void createsMotorsAndSensors() {
		brick = BrickPi.getBrickPi();
		brick.setUpdateDelay(2);
		move = new Motor();
		pump = new Motor();
		pumpControl = new Motor();
		touchSensor = new Sensor(SensorType.Raw);
		glasSensor = new Sensor(SensorType.Raw);
	}

	public static void init() {
		createsMotorsAndSensors();

		brick.setMotor(move, MotorPort.MD);
		brick.setMotor(pumpControl, MotorPort.MC);
		brick.setMotor(pump, MotorPort.MB);

		brick.setSensor(touchSensor, SensorPort.S3);
		brick.setSensor(glasSensor, SensorPort.S4);

		try {
			brick.setupSensors();
		} catch (IOException e) {
			System.out.println("Error to initialize Robot");
			e.printStackTrace();
		}

		move.resetEncoder();
		pump.resetEncoder();
		pumpControl.resetEncoder();
	}
}
