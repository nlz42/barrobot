package barRobot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import barRobot.configuration.RobotPortConfiguration;
import barRobot.querysensor.QuerySensor;
import barRobot.robotControl.Barkeeper;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		// start Robot Configuration
		RobotPortConfiguration.init();
		// start Query Sensor Values
		Thread querySensors = new Thread(new QuerySensor());
		querySensors.start();
		Thread barkeeper = new Thread(new Barkeeper());
		barkeeper.start();

	}
}
