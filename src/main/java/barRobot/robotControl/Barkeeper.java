package barRobot.robotControl;

import barRobot.domain.SocketMessage;
import barRobot.querysensor.QuerySensor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * This Class is use for autnomous drink mixing
 *
 * @author NBott
 */
@Component
public class Barkeeper implements Runnable {

    private static SimpMessageSendingOperations messagingTemplate;

    public String status = "waiting";
    public static String drinkOrder = "empty";
    public String curPosition = "start";
    public static boolean busy = false;
    public Map<String, Integer> drinkMap = new HashMap<String, Integer>();

    public Barkeeper() {
        drinkMap.put("start", 0);
        drinkMap.put("gin", 1);
        drinkMap.put("rum", 3);
        drinkMap.put("whiskey", 5);
        drinkMap.put("tequila", 7);
    }

    public static void setMessagingTemplate(SimpMessageSendingOperations newMessagingTemplate) {
        messagingTemplate = newMessagingTemplate;
    }

    @Override
    public void run() {

        while (true) {
            if (!(drinkOrder.equals("empty"))) {
                mixDrink();
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void mixDrink() {
        busy = true;
        try {
            stompSender(new SocketMessage(Boolean.toString(busy)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        // JackyCola
        switch (drinkOrder) {
            case "gin":
                moveTo("gin");
                drawDrink();
                moveTo("start");
                break;
            case "rum":
                moveTo("rum");
                drawDrink();
                moveTo("start");
                break;
            case "whiskey":
                moveTo("whiskey");
                drawDrink();
                moveTo("start");
                break;
            case "tequila":
                moveTo("tequila");
                drawDrink();
                moveTo("start");
                break;
            default:
                drinkOrder = "empty";
                break;
        }
        drinkOrder = "empty";
        busy = false;
        try {
            stompSender(new SocketMessage(Boolean.toString(busy)));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void moveTo(String position) {
        int curPosition = QuerySensor.getPosition();
        int destinationPosition = drinkMap.get(position);
        while (curPosition != destinationPosition) {
            while (curPosition != destinationPosition) {
                if (curPosition < destinationPosition) {
                    MoveRobot.forward();
                } else {
                    MoveRobot.backward();
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                curPosition = QuerySensor.getPosition();
            }
            MoveRobot.stop();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MoveRobot.setSpeed(45);
            curPosition = QuerySensor.getPosition();
        }
        MoveRobot.setSpeed(80);
        // move a little bit to start postion
        if (destinationPosition == 0) {
            MoveRobot.backward();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        MoveRobot.stop();
    }

    public String getDrinkOrder() {
        return drinkOrder;
    }

    public static void setDrinkOrder(String drinkOrder) {
        System.out.println("Order: " + drinkOrder);
        Barkeeper.drinkOrder = drinkOrder;
    }

    private void drawDrink() {
        try {
            MoveRobot.setPumpControlToUp();
            Thread.sleep(250);
            MoveRobot.pumpGlassUp();
            Thread.sleep(3000);
            MoveRobot.setPumpControlToDown();
            Thread.sleep(250);
            MoveRobot.pumpGlassDown();
            for (int i = 0; i < 3; i++) {
                if (QuerySensor.getGlasSensor() == 0) {
                    selfHealing();
                }
            }
            if (QuerySensor.getGlasSensor() == 0) {
                // Glas wont go down correct
                System.exit(1);// shutdown service complete!
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stompSender(SocketMessage msg) throws Exception {
        System.out.println("Send Message from barkeeper: " + msg.getContent());
        messagingTemplate.convertAndSend("/topic/stompMessage", msg);
    }

    private void selfHealing() {
        try {
            MoveRobot.setPumpControlToUp();
            Thread.sleep(250);
            MoveRobot.setPumpControlToDown();
            Thread.sleep(250);
            MoveRobot.setPumpControlToUp();
            Thread.sleep(250);
            MoveRobot.setPumpControlToDown();
            Thread.sleep(250);
            MoveRobot.pumpGlassDown();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
