package barRobot.robotControl;

import java.awt.event.KeyEvent;

import barRobot.configuration.RobotPortConfiguration;
import barRobot.querysensor.QuerySensor;

/**
 * This class is use to prompt the Roboter to do something.
 *
 * @author NBott
 */
@SuppressWarnings("static-access")
public class MoveRobot {

	private static RobotPortConfiguration robot;
	private static String lastMovedDirection="stop";
	private static int speed=80;
	public static void move(String key) {
		char c = key.charAt(0);

		switch (c) {
		case 'Q':
			pump();
			break;
		case 'Y':
			setPumpControlToDown();
			break;
		case 'A':
			setPumpControlToUp();
			break;
		case KeyEvent.VK_RIGHT:
			forward();
			break;
		case KeyEvent.VK_LEFT:
			backward();
			break;
		case 'O':
			pumpGlassUp();
			break;
		case 'P':
			pumpGlassDown();
			break;
		case 'R':
			stop();
			break;
		default:
			stop();
		}
	}

	public static void pump() {
		robot.pump.rotate(99.0, 200);
	}

	public static void forward() {
		lastMovedDirection="forward";
		if(QuerySensor.getPosition()>8) {
			stop();
		} else {
			robot.move.rotate(99, speed);
		}	
	}

	public static void backward() {
		lastMovedDirection="backward";
		robot.move.rotate(99, -speed);
	}

	public static void setPumpControlToUp() {
		 // 1 umdrehung =  1440 => 1080 (3/4), delay 180 uodate value diff
		while (robot.pumpControl.getCurrentEncoderValue() < 900) {
			robot.pumpControl.rotate(0.1, 55);
		}
	}

	public static void setPumpControlToDown() {
		while (robot.pumpControl.getCurrentEncoderValue() > 180) {
			robot.pumpControl.rotate(0.1, -55);
		}
	}

	public static void pumpGlassUp() {
		QuerySensor.resetPumpEncoder();
		sleepMs(100);
		// 1440 * 25 = 36000
		while (QuerySensor.getPump() < 40000) {
			robot.pump.rotate(25.0, 230);
		}
		stop();
	}

	public static void pumpGlassDown() {
		QuerySensor.resetPumpEncoder();
		sleepMs(100);
		// 1440 * 6 = 8640
		while (QuerySensor.getPump() < 8640) {
			robot.pump.rotate(25.0, 230);
		}
		stop();
	}

	public static void stop() {
		while (robot.pump.getCurrentSpeed() != 0 || robot.move.getCurrentSpeed() != 0 || robot.pumpControl.getCurrentSpeed() != 0) {
			robot.move.rotate(0, 0);
			robot.pump.rotate(0, 0);
			robot.pumpControl.rotate(0, 0);
			sleepMs(50);
		}
	}

	public static String getLastMovedDirection() {
		return lastMovedDirection;
	}

	public int getSpeed() {
		return speed;
	}

	public static void setSpeed(int speed) {
		MoveRobot.speed = speed;
	}	
	
	private static void sleepMs(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
