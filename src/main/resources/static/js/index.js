var stompClient = null;
var busy = "false";

function connect() {
	try {
		var socket = new SockJS(window.location.protocol + "//"
				+ window.location.hostname + ":" + window.location.port
				+ "/barrobot-websocket");
		stompClient = Stomp.over(socket);
		stompClient.connect({}, connect_callback, error_callback);
	} catch (err) {
		$("#websocket-logs").prepend(
				"<tr><td>" + getDate() + "</td><td>" + err.message
						+ "</td></tr>");
	}
}

function disconnect() {
	if (stompClient != null) {
		stompClient.disconnect();
	}

	setConnected(false);
}

function sendDirection() {
	stompClient.send("/app/stompMessage", {}, JSON.stringify({
		'content' : arguments[0]
	}));
}

function setConnected(connected) {
	$("#connect").prop("disabled", connected);
	$("#disconnect").prop("disabled", !connected);
	if (connected) {
		$("#connected").attr("class", "alert alert-success");
		$("#connected").text("Connected");
	} else {
		$("#connected").attr("class", "alert alert-danger");
		$("#connected").text("Disconnected");
	}
}

function getDate() {
	var date = new Date();
	return date.getDate() + "." + date.getMonth() + "." + date.getFullYear()
			+ ", " + date.getHours() + ":" + date.getMinutes() + ":"
			+ date.getSeconds();
}

var connect_callback = function() {
	stompClient.subscribe('/topic/stompMessage', function(message) {
		$("#connected").text(JSON.parse(message.body).content);
		busy = JSON.parse(message.body).content;
		update_isBusy();
		console.log("MSG vom Typ: " + typeof msg + " Meaasge: " + busy);
	});

	setConnected(true);
};

var error_callback = function(error) {
	$("#websocket-logs").prepend(
			"<tr class=\"normal-text\"><td>" + getDate() + "</td><td>" + error + "</td></tr>");
	setTimeout(connect, 10000);
	setConnected(false);
}

function update_isBusy() {
	if (busy == "true" || busy == "false") {

		$("#busy").text(busy);
		if (busy == "true") {
			$('#orderRoom').hide();
			$('#waitingRoom').show();
			console.log("It is TRUE ");
		} else {
			$('#waitingRoom').hide();
			$('#orderRoom').show();
			console.log("It is FALSE ");
		}
	}
}

function initalGetBusyParam () {
    $.get("/getBusy", function (data, status) {
        console.log("Busy Var is: " + data + "\nStatus: " + status);
        busy=data;
        update_isBusy()
    });
};

$(function() {
	// first hide order romm, until busy var is checked
	$('#orderRoom').hide();	
	// connect to brickpi-websocket
	connect();
	// check if is busy
	initalGetBusyParam();

	// buttons
	$("form").on('submit', function(e) {
		e.preventDefault();
	});
	$("#connect").click(function() {
		connect();
	});
	$("#disconnect").click(function() {
		disconnect();
	});

	$("#gin").click(function() {
		sendDirection("gin");
	});
	$("#rum").click(function() {
		sendDirection("rum");
	});
	$("#whiskey").click(function() {
		sendDirection("whiskey");
	});
	$("#tequila").click(function() {
		sendDirection("tequila");
	});


	// key events
	$('body').on(
			'keydown',
			function(e) {
				var key = String.fromCharCode(e.which);
				console.log("Keypress Type: " + e.type + "\nKeypress Char: "
						+ e.which + " which is a " + key);
				if (stompClient != null) {
					sendDirection(key);
				}
			});

	$('body').on('keyup', function(e) {
		console.log("Keypress Type: " + e.type);
		if (stompClient != null) {
			sendDirection("stop");
		}
	});
});

$(window).load(function() {
	// Animate loader off screen
	$(".se-pre-con").fadeOut("slow");;
});
