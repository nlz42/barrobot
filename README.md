# BarRobot

Lego Bar Robot with Spring MVC, Websocket, MJPEG Streamer, BrickPi, JS, Java

## Getting Started

### Connecting all Wires:

- Motor to **move** forwar-backward: Port **MD**
- Motor to **pump**: Port **MB**
- Motor to set **Up/Down** platform: Port **MC**
- Touch Sensor for **positioning**: Port **S3**
- Touch Sensor for **IsGlasDown**: Port **S4**

All other ports are free.

Free Ports: **MA,S1,S2**

### Start Application

Just start the Application on an RaspberryPi connected to a BrickPi with java -jar BarRobot-0.0.1-SNAPSHOT.jar .

The Web Application is running on: http://host:8090

##What to do

- [x] Write basic Code to Control Motors
- [x] Read out Motor Sensor Datas
- [x] Write web application (Basic)
- [x] Testphase: How much rotations i need to get water out
- [x] Write functions like pumpControlToUp etc for control with keys
- [x] Key control for the Robot. Test if it works
- [x] Connected TouchSensor to determine the position
- [x] Read out Sensor
- [x] Test with key control (position determine in focus)
- [x] Write first autonomous function to get first drink
- [x] Coding the other drinks down
- [x] Big longterm Test - Bugfixing
- [x] Enjoy the life by drinking drinks without have to mixed them ;-)


## KeyControl

* **Q** pump on
* **Y** pump to state: DOWN
* **A** pump to state: UP
* **Arrow right**: move forward
* **Arrow left**: move backward
* **R** to stop all
* **O** pump glass up
* **P** pump glass down

## Autonomous function

Order a drink over the web site, the autonomous Service start to mix a drink for you.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Nils Bott** - *Hobby Project* -


